/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import 'react-native-gesture-handler';

import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import {NavigationContainer} from '@react-navigation/native';

import {createStackNavigator} from '@react-navigation/stack';

import SplashScreen from './src/splash';

import Login from './src/LoginScreen';

import otpVerify from './src/otpVerify';

import Register from './src/Register';

const Stack = createStackNavigator();
export default class App extends Component {
  constructor() {
    super();
    this.state = {
      isVisible: true,
    };
  }

  Hide_splash_screen = () => {
    this.setState({
      isVisible: false,
    });
  };

  componentDidMount() {
    let that = this;
    setTimeout(function() {
      that.Hide_splash_screen();
    }, 6000);
  }

  render() {
    return (
      <NavigationContainer>
        {this.state.isVisible == true ? (
          <Stack.Navigator headerMode="none">
            <Stack.Screen name="SplashScreen" component={SplashScreen} />
          </Stack.Navigator>
        ) : (
          <Stack.Navigator headerMode="none">
            <Stack.Screen name="Login" component={Login} />
            <Stack.Screen name="Register" component={Register} />
            <Stack.Screen name="otpVerify" component={otpVerify} />
          </Stack.Navigator>
        )}
      </NavigationContainer>
    );
  }
}
const styles = StyleSheet.create({
  childView: {
    backgroundColor: '#fff',
  },
});
