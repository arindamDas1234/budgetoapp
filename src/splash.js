import React, {Component} from 'react';

import {StyleSheet, Text, View, Textinput, Image} from 'react-native';

export default class SplashScreen extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <View style={styles.ScreenStyle_root}>
        <View style={styles.childView}>
          <Image
            source={require('./images/bageto.png')}
            style={{width: '100%', height: '100%', resizeMode: 'contain'}}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  childView: {
    backgroundColor: '#fff',
  },
});
