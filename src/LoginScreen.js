import React, {Component} from 'react';

import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  TouchableOpacity,
} from 'react-native';

import {TextInput} from 'react-native-paper';

import {Button} from 'react-native-paper';

import NetInfo from '@react-native-community/netinfo';

import {AsyncStorage} from 'react-native';

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      user_id: '',
    };
  }
  //  email validation

  validationEmail = email => {
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
  };

  Login = () => {
    if (this.state.email == '' && this.state.password) {
      alert('Pleace Enter Field The Form Requirement');
    } else if (!this.validationEmail(this.state.email)) {
      alert('Email Formate is Invalid');
    } else {
      NetInfo.fetch().then(status => {
        if (status.isConnected) {
          fetch(
            'http://vmi317167.contaboserver.net/ecommerce/api/customer_login',
            {
              method: 'POST',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded',
              },
              body:
                'username=' +
                this.state.email +
                '&password=' +
                this.state.password,
            },
          )
            .then(response => response.json())
            .then(res => {
              // console.log(res.data.id);
              // return;
              if (res.flag == 1) {
                try {
                  AsyncStorage.setItem('user_id', JSON.stringify(res.data.id));
                } catch (error) {}
                alert(res.data);
                // try {
                //   console.log(AsyncStorage.getItem('user_id'));
                // } catch (error) {
                //   console.log(error);
                // }
                this.props.navigation.replace('otpVerify');
              }
            });
        } else {
          alert('Pleace Check your  Enternet connection');
        }
      });
    }
  };
  render() {
    return (
      <View style={styles.ViewContainer}>
        <StatusBar
          barStyle="light-content"
          // dark-content, light-content and default
          hidden={false}
          //To hide statusBar
          backgroundColor="#4a148c"
          //Background color of statusBar
          translucent={false}
          //allowing light, but not detailed shapes
          networkActivityIndicatorVisible={true}
        />
        <Text style={styles.HeaderText}>Login Account</Text>

        <TextInput
          label="Email"
          onChangeText={email => this.setState({email})}
          style={styles.InputField}
        />
        <TextInput
          label="Password"
          secureTextEntry={true}
          onChangeText={password => this.setState({password})}
          style={styles.InputField}
        />

        <Button
          mode="contained"
          style={styles.ButtonField}
          onPress={this.Login}>
          Signin
        </Button>

        <TouchableOpacity
          onPress={() => this.props.navigation.replace('Register')}>
          <Text style={styles.OpacityText}>
            Don't have Any Account ? Create One
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  ViewContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  HeaderText: {
    paddingTop: 40,
    fontSize: 30,
    fontWeight: 'normal',
    marginBottom: 30,
  },
  InputField: {
    width: 300,
    marginBottom: 25,
    height: 70,
  },
  ButtonField: {
    width: 200,
    borderRadius: 3,
    elevation: 5,
  },
  OpacityText: {
    paddingTop: 25,
    color: '#4a148c',
  },
});
