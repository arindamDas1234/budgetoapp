import React, {useState, Component} from 'react';

import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  StatusBar,
} from 'react-native';

const routerProps = props => props;
import {Button, KeyboardAvoidingView} from 'react-native-paper';
import {ActivityIndicator, Colors} from 'react-native-paper';

import {AsyncStorage} from 'react-native';

import NetInfo from '@react-native-community/netinfo';

export default class otpVerify extends Component {
  constructor(props) {
    super(props);
    this.state = {
      number: '',
      user_id: '',
      isClickLoading: true,
      isNavigate: this.props.navigation,
    };
  }

  // componentDidMount() {
  //   console.log(AsyncStorage.getItem('user_id'));
  // }

  Login = async () => {
    // try {
    //   var id = await AsyncStorage.getItem('user_id');
    //   console.log(id);
    // } catch (error) {
    //   console.log(error);
    // }
    // return;
    // console.log(this.state.number);
    // return;
    // NetInfo.isConnected.addEventListener(
    //   'connectionChange',
    //   this._handleConnectivityChange,
    // );
    // console.log('Hello World');
    if (this.state.number == '') {
      alert('Pleace Enter Your Valid Number');
    } else if (this.state.number.length < 10) {
      alert('Your Phone Number is Invalid');
    } else {
      NetInfo.fetch().then(state => {
        // console.log(state.isConnected);
        if (state.isConnected) {
          this.setState({
            isClickLoading: false,
          });

          fetch('http://192.168.42.113:4000/otp-send', {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              number: this.state.number,
            }),
          })
            .then(result => result.json())
            .then(response => {
              this.setState({
                isClickLoading: true,
              });
              // console.log(response.messages);
              // return;

              if (response.success) {
                alert(response.messages);
                //    navigate to opt verify page
                this.props.navigation.replace('otpVerify');
                // console.log('success');
                // alert('success fully Login');
              } else {
                alert(response.messages);
              }
            })
            .catch(error => {
              console.log(error);
            });
        } else {
          alert('No internet connection');
        }
      });
    }
  };

  checkClick = () => {
    if (this.state.isClickLoading == true) {
      return true;
    } else {
      return false;
    }
  };
  render() {
    return (
      <View style={styles.container}>
        <StatusBar
          barStyle="light-content"
          // dark-content, light-content and default
          hidden={false}
          //To hide statusBar
          backgroundColor="#4a148c"
          //Background color of statusBar
          translucent={false}
          //allowing light, but not detailed shapes
          networkActivityIndicatorVisible={true}
        />
        <Text style={styles.headerText}>
          To Place Order Verify Your Phone Number
        </Text>
        <TextInput
          style={styles.InputText}
          placeholder="Enter Your Valid Number"
          keyBoardType="number-pad"
          onChangeText={number => this.setState({number})}
        />

        <TouchableOpacity style={styles.Send_otp} onPress={this.Login}>
          {this.checkClick ? (
            <Text style={styles.buttonText}>SEND OTP</Text>
          ) : (
            <ActivityIndicator color="#ffffff" />
          )}
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 60,
  },
  headerText: {
    paddingTop: 30,
    paddingLeft: 9,
    fontSize: 20,
  },
  textButton: {
    marginTop: 20,
  },
  InputText: {
    backgroundColor: '#e0e0e0',
    width: 300,
    marginTop: 30,
  },
  Send_otp: {
    backgroundColor: '#6a1b9a',
    width: 200,
    height: 35,
    marginTop: 30,
    borderWidth: 1,
    borderRadius: 4,
    elevation: 5,
  },
  buttonText: {
    color: '#fff',
    paddingLeft: 65,
    marginTop: 6,
  },
});
